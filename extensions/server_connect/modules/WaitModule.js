exports.Wait = async function (options) {
    options = this.parse(options);
    //https://www.sitepoint.com/delay-sleep-pause-wait/
    return new Promise(resolve => setTimeout(resolve, options.value));
}